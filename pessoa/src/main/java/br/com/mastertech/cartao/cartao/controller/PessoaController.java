package br.com.mastertech.cartao.cartao.controller;

import br.com.mastertech.cartao.cartao.clients.CarroClient;
import br.com.mastertech.cartao.cartao.clients.Cep;
import br.com.mastertech.cartao.cartao.model.Carro;
import br.com.mastertech.cartao.cartao.model.Pessoa;
import br.com.mastertech.cartao.cartao.clients.CepClient;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PessoaController {

    @Autowired
    private CarroClient carroClient;

    @Autowired
    private CepClient cepClient;

    @GetMapping("/{nome}/{placa}")
    public Pessoa create(@PathVariable String nome, @PathVariable String placa) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(nome);

        Carro carroByPlaca = carroClient.getCarroByPlaca(placa);

        pessoa.setCarro(carroByPlaca);

        return pessoa;
    }

    @GetMapping("/{cep}")
    public Cep getCep(@PathVariable String cep) {
        try {
            return cepClient.getByCep(cep);
        } catch (FeignException.BadRequest e) {
            return new Cep();
        }
    }


}
