package br.com.mastertech.cartao.cartao.clients;

import br.com.mastertech.cartao.cartao.model.Carro;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "carro")
public interface CarroClient {

    @GetMapping("/{placa}")
    Carro getCarroByPlaca(@PathVariable String placa);

}
