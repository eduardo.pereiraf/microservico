package com.payment.payment.service;

import com.payment.payment.models.Payment;
import com.payment.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

//    @Autowired
//    private CreditCardService creditCardService;

    public Payment create(Payment payment) {
//        CreditCard creditCard = creditCardService.getById(payment.getCreditCard().getId());

//        payment.setCreditCard(creditCard);

        return paymentRepository.save(payment);
    }

    public List<Payment> findAllByCreditCard(Long creditCardId) {
        return paymentRepository.findAllByCreditCard_id(creditCardId);
    }

}