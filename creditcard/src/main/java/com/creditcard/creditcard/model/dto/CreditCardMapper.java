package com.creditcard.creditcard.model.dto;

import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {

    public CreditCard toCreditCard(CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(createCreditCardRequest.getNumber());
        creditCard.setCustomerId(createCreditCardRequest.getCustomerId());

        return creditCard;
    }

    public CreateCreditCardResponse toCreateCreditCardResponse(CreditCard creditCard) {
        CreateCreditCardResponse createCreditCardResponse = new CreateCreditCardResponse();

        createCreditCardResponse.setId(creditCard.getCustomerId());
        createCreditCardResponse.setNumber(creditCard.getNumber());
        createCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        createCreditCardResponse.setActive(creditCard.getActive());

        return createCreditCardResponse;
    }

    public CreditCard toCreditCard(UpdateCreditCardRequest updateCreditCardRequest) {
        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(updateCreditCardRequest.getNumber());
        creditCard.setActive(updateCreditCardRequest.getActive());

        return creditCard;
    }

    public UpdateCreditCardResponse toUpdateCreditCardResponse(CreditCard creditCard) {
        UpdateCreditCardResponse updateCreditCardResponse = new UpdateCreditCardResponse();

        updateCreditCardResponse.setId(creditCard.getCustomerId());
        updateCreditCardResponse.setNumber(creditCard.getNumber());
        updateCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        updateCreditCardResponse.setActive(creditCard.getActive());

        return updateCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard) {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();

        getCreditCardResponse.setId(creditCard.getCustomerId());
        getCreditCardResponse.setNumber(creditCard.getNumber());
        getCreditCardResponse.setCustomerId(creditCard.getCustomerId());

        return getCreditCardResponse;
    }
}
