package br.com.mastertech.cartoes.customer.models.dto;

import br.com.mastertech.cartoes.customer.models.Customer;

public class CreateCustomerRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
