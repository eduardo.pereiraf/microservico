package com.payment.payment.repository;

import com.payment.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByCreditCard_id(Long creditCardId);
}
