package com.creditcard.creditcard.service;

import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import com.creditcard.creditcard.exceptions.CreditCardNotFoundException;
import com.creditcard.creditcard.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

//    @Autowired
//    private CustomerService customerService;

    public CreditCard create(CreditCard creditCard) {
//        Customer customer = customerService.getById(creditCard.getCustomer().getId());

//        creditCard.setCustomer(customer);
        creditCard.setActive(false);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard creditCard) {
        CreditCard databaseCreditCard = getByNumber(creditCard.getNumber());

        databaseCreditCard.setActive(creditCard.getActive());

        return creditCardRepository.save(databaseCreditCard);
    }

    public CreditCard getByNumber(String number) {
        /* Exemplo em 1 linha
        CreditCard creditCard = creditCardRepository.findByNumber(number)
                .orElseThrow(CreditCardNotFoundException::new);
        */

        // nosso código normal
        Optional<CreditCard> byId = creditCardRepository.findByNumber(number);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

}